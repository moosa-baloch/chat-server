#Chat Application#
# Server Repo #
[Visit For Client Repo](https://bitbucket.org/moosa-baloch/chat-client/)

**Platform: Android**

**Database: Firebase**

**Server: Node.js for GCM**

**Authentication: Firebase **

* ##Why Chat Application:
Developing chat application with Firebase and GCM client server architecture is a bit difficult
and unique task that's why I have learned JavaScript first then Node server in this mean time
then I have implemented the complete architecture to build this application.

* ##Application Architecture:
![Diagram](https://bitbucket.org/repo/yGpyrb/images/1110543973-New%20Network%20Diagrams%20-%20Demo%20%281%29.png)
  
* ##How Application works:
User opens the application and sign-in to his/her account if account was not previously 
exist then the user have to sign-up to use this application and then sign-in to use it.
When the user is logged-in there are 4 tabs and Navigation Drawer for user Profile.
when the user open drawer they can set profile picture with the icon hint upload 
the picture was uploaded at Cloudinary.com and the url is saved at Firebase user can 
can see the picture when it was uploaded successfully. 
Now there are 4 tabs the first or the left most tab is for the current conversations 
only user can see all the current conversation on the chat application not from the group
Now the second tab is for group communication in this tab user can see only the joined groups
for joinging any group or to create any group user have to click the FAB button to join or create 
the group. After clicking the FAB add button in group tab the new screen appears in which user
 can see all the groups available on this chat application by clicking the right side
button in group the user joined the desired group. user can notice there is another add button 
on this screen by which user can create a new group on Chat application.
Now the third tab is for to view all the friends user have. user can see the profile details 
or start conversation to the friends added here. user can add a new friend by clicking on FAB
button below. in which the new screen appears and the user can see all the peoples available
on chat app. Now the Forth tab is only for Friend request user can check the friend request 
here and can accept or delete the friend request.
## NOTE:
* In all the application user can receive notification from any friend who text him.or he can also receive notification from group chat and also receive notification from the 
user who sends the request. 
* If one user messages the other user then notification is generated and if the same user 
messages twice then the only one notification is shown.
* At last when the application is in running state the notification is not generated.
notification is only generated when the user is not using an application or destroys it from 
running application.